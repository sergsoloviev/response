package response

import (
	"encoding/json"
	"net/http"
)

type ResponseJSON struct {
	Error interface{} `json:"error"`
	Data  interface{} `json:"data"`
	Next  interface{} `json:"next"`
	Prev  interface{} `json:"prev"`
	Count int         `json:"count"`
	Pages int         `json:"pages"`
	Page  int         `json:"page"`
	Refs  interface{} `json:"refs"`
}

func (res *ResponseJSON) Render(w http.ResponseWriter, StatusCode int) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(StatusCode)
	json.NewEncoder(w).Encode(res)
}
